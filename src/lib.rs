/*
    Creative Commons Legal Code

    CC0 1.0 Universal

        CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
        LEGAL SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN
        ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS
        INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES
        REGARDING THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
        PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM
        THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS PROVIDED
        HEREUNDER.

    Statement of Purpose

    The laws of most jurisdictions throughout the world automatically confer
    exclusive Copyright and Related Rights (defined below) upon the creator
    and subsequent owner(s) (each and all, an "owner") of an original work of
    authorship and/or a database (each, a "Work").

    Certain owners wish to permanently relinquish those rights to a Work for
    the purpose of contributing to a commons of creative, cultural and
    scientific works ("Commons") that the public can reliably and without fear
    of later claims of infringement build upon, modify, incorporate in other
    works, reuse and redistribute as freely as possible in any form whatsoever
    and for any purposes, including without limitation commercial purposes.
    These owners may contribute to the Commons to promote the ideal of a free
    culture and the further production of creative, cultural and scientific
    works, or to gain reputation or greater distribution for their Work in
    part through the use and efforts of others.

    For these and/or other purposes and motivations, and without any
    expectation of additional consideration or compensation, the person
    associating CC0 with a Work (the "Affirmer"), to the extent that he or she
    is an owner of Copyright and Related Rights in the Work, voluntarily
    elects to apply CC0 to the Work and publicly distribute the Work under its
    terms, with knowledge of his or her Copyright and Related Rights in the
    Work and the meaning and intended legal effect of CC0 on those rights.

    1. Copyright and Related Rights. A Work made available under CC0 may be
    protected by copyright and related or neighboring rights ("Copyright and
    Related Rights"). Copyright and Related Rights include, but are not
    limited to, the following:

      i. the right to reproduce, adapt, distribute, perform, display,
         communicate, and translate a Work;
     ii. moral rights retained by the original author(s) and/or performer(s);
    iii. publicity and privacy rights pertaining to a person's image or
         likeness depicted in a Work;
     iv. rights protecting against unfair competition in regards to a Work,
         subject to the limitations in paragraph 4(a), below;
      v. rights protecting the extraction, dissemination, use and reuse of data
         in a Work;
     vi. database rights (such as those arising under Directive 96/9/EC of the
         European Parliament and of the Council of 11 March 1996 on the legal
         protection of databases, and under any national implementation
         thereof, including any amended or successor version of such
         directive); and
    vii. other similar, equivalent or corresponding rights throughout the
         world based on applicable law or treaty, and any national
         implementations thereof.

    2. Waiver. To the greatest extent permitted by, but not in contravention
    of, applicable law, Affirmer hereby overtly, fully, permanently,
    irrevocably and unconditionally waives, abandons, and surrenders all of
    Affirmer's Copyright and Related Rights and associated claims and causes
    of action, whether now known or unknown (including existing as well as
    future claims and causes of action), in the Work (i) in all territories
    worldwide, (ii) for the maximum duration provided by applicable law or
    treaty (including future time extensions), (iii) in any current or future
    medium and for any number of copies, and (iv) for any purpose whatsoever,
    including without limitation commercial, advertising or promotional
    purposes (the "Waiver"). Affirmer makes the Waiver for the benefit of each
    member of the public at large and to the detriment of Affirmer's heirs and
    successors, fully intending that such Waiver shall not be subject to
    revocation, rescission, cancellation, termination, or any other legal or
    equitable action to disrupt the quiet enjoyment of the Work by the public
    as contemplated by Affirmer's express Statement of Purpose.

    3. Public License Fallback. Should any part of the Waiver for any reason
    be judged legally invalid or ineffective under applicable law, then the
    Waiver shall be preserved to the maximum extent permitted taking into
    account Affirmer's express Statement of Purpose. In addition, to the
    extent the Waiver is so judged Affirmer hereby grants to each affected
    person a royalty-free, non transferable, non sublicensable, non exclusive,
    irrevocable and unconditional license to exercise Affirmer's Copyright and
    Related Rights in the Work (i) in all territories worldwide, (ii) for the
    maximum duration provided by applicable law or treaty (including future
    time extensions), (iii) in any current or future medium and for any number
    of copies, and (iv) for any purpose whatsoever, including without
    limitation commercial, advertising or promotional purposes (the
    "License"). The License shall be deemed effective as of the date CC0 was
    applied by Affirmer to the Work. Should any part of the License for any
    reason be judged legally invalid or ineffective under applicable law, such
    partial invalidity or ineffectiveness shall not invalidate the remainder
    of the License, and in such case Affirmer hereby affirms that he or she
    will not (i) exercise any of his or her remaining Copyright and Related
    Rights in the Work or (ii) assert any associated claims and causes of
    action with respect to the Work, in either case contrary to Affirmer's
    express Statement of Purpose.

    4. Limitations and Disclaimers.

     a. No trademark or patent rights held by Affirmer are waived, abandoned,
        surrendered, licensed or otherwise affected by this document.
     b. Affirmer offers the Work as-is and makes no representations or
        warranties of any kind concerning the Work, express, implied,
        statutory or otherwise, including without limitation warranties of
        title, merchantability, fitness for a particular purpose, non
        infringement, or the absence of latent or other defects, accuracy, or
        the present or absence of errors, whether or not discoverable, all to
        the greatest extent permissible under applicable law.
     c. Affirmer disclaims responsibility for clearing rights of other persons
        that may apply to the Work or any use thereof, including without
        limitation any person's Copyright and Related Rights in the Work.
        Further, Affirmer disclaims responsibility for obtaining any necessary
        consents, permissions or other rights required for any use of the
        Work.
     d. Affirmer understands and acknowledges that Creative Commons is not a
        party to this document and has no duty or obligation with respect to
        this CC0 or use of the Work.
*/

/*!
    Enum types with fully-generic variants.

    This library defines enum types which are generic over from 2 ([`Enum2`]) to 16 ([`Enum16`]) type parameters.
    Each type parameter of a given enum type is the type of the single field of its respective variant of the same name
    (all variants are tuple structs with a single field).

    ```
    use std::collections::HashMap;

    use anon_enum::Enum2;

    /// Error type stating that a map does not contain a given key.
    struct ErrKeyNotFound;

    /// Error type stating that an integer overflow occured during
    /// an arithmetic operation.
    struct ErrIntegerOverflow;

    /// Maps a `str` to an integer of type `i32` and returns the value plus one.
    fn get_and_add_one(
        map: &HashMap<&str, i32>,
        key: &str,
    ) -> Result<i32, Enum2<ErrKeyNotFound, ErrIntegerOverflow>> {
        Ok(map
            .get(key)
            .ok_or(Enum2::T0(ErrKeyNotFound))?
            .checked_add(1)
            .ok_or(Enum2::T1(ErrIntegerOverflow))?)
    }

    fn main() {
        let mut fruit_count = HashMap::new();

        fruit_count.insert("Lime", 5);
        fruit_count.insert("Orange", i32::MAX);

        matches!(get_and_add_one(&fruit_count, "Lime"), Ok(6));
        matches!(
            get_and_add_one(&fruit_count, "Tangerine"),
            Err(Enum2::T0(ErrKeyNotFound))
        );
        matches!(
            get_and_add_one(&fruit_count, "Orange"),
            Err(Enum2::T1(ErrIntegerOverflow))
        );
    }
    ```

    An enum of more than 16 variants may be expressed by setting
    the generic type `T15` of [`Enum16`] to an enum type:

    ```rust,ignore
    // Enum of 18 variants.
    Enum16<T0, T1, ..., T14, Enum3<T15, T16, T17>>
    ```
*/

#![forbid(unsafe_code)]
#![no_std]

#[derive(Debug)]
pub enum Enum2<T0, T1> {
    T0(T0),
    T1(T1),
}

impl<T, T0, T1> AsMut<T> for Enum2<T0, T1>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1> AsRef<T> for Enum2<T0, T1>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum3<T0, T1, T2> {
    T0(T0),
    T1(T1),
    T2(T2),
}

impl<T, T0, T1, T2> AsMut<T> for Enum3<T0, T1, T2>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2> AsRef<T> for Enum3<T0, T1, T2>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum4<T0, T1, T2, T3> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
}

impl<T, T0, T1, T2, T3> AsMut<T> for Enum4<T0, T1, T2, T3>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3> AsRef<T> for Enum4<T0, T1, T2, T3>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum5<T0, T1, T2, T3, T4> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
}

impl<T, T0, T1, T2, T3, T4> AsMut<T> for Enum5<T0, T1, T2, T3, T4>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4> AsRef<T> for Enum5<T0, T1, T2, T3, T4>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum6<T0, T1, T2, T3, T4, T5> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
}

impl<T, T0, T1, T2, T3, T4, T5> AsMut<T> for Enum6<T0, T1, T2, T3, T4, T5>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5> AsRef<T> for Enum6<T0, T1, T2, T3, T4, T5>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum7<T0, T1, T2, T3, T4, T5, T6> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
}

impl<T, T0, T1, T2, T3, T4, T5, T6> AsMut<T> for Enum7<T0, T1, T2, T3, T4, T5, T6>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6> AsRef<T> for Enum7<T0, T1, T2, T3, T4, T5, T6>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum8<T0, T1, T2, T3, T4, T5, T6, T7> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7> AsMut<T> for Enum8<T0, T1, T2, T3, T4, T5, T6, T7>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7> AsRef<T> for Enum8<T0, T1, T2, T3, T4, T5, T6, T7>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum9<T0, T1, T2, T3, T4, T5, T6, T7, T8> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
    T8(T8),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8> AsMut<T> for Enum9<T0, T1, T2, T3, T4, T5, T6, T7, T8>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
    T8: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
            Self::T8(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8> AsRef<T> for Enum9<T0, T1, T2, T3, T4, T5, T6, T7, T8>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
    T8: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
            Self::T8(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
    T8(T8),
    T9(T9),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> AsMut<T>
    for Enum10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
    T8: AsMut<T>,
    T9: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
            Self::T8(t) => t.as_mut(),
            Self::T9(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> AsRef<T>
    for Enum10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
    T8: AsRef<T>,
    T9: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
            Self::T8(t) => t.as_ref(),
            Self::T9(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
    T8(T8),
    T9(T9),
    T10(T10),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> AsMut<T>
    for Enum11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
    T8: AsMut<T>,
    T9: AsMut<T>,
    T10: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
            Self::T8(t) => t.as_mut(),
            Self::T9(t) => t.as_mut(),
            Self::T10(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> AsRef<T>
    for Enum11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
    T8: AsRef<T>,
    T9: AsRef<T>,
    T10: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
            Self::T8(t) => t.as_ref(),
            Self::T9(t) => t.as_ref(),
            Self::T10(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
    T8(T8),
    T9(T9),
    T10(T10),
    T11(T11),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> AsMut<T>
    for Enum12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
    T8: AsMut<T>,
    T9: AsMut<T>,
    T10: AsMut<T>,
    T11: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
            Self::T8(t) => t.as_mut(),
            Self::T9(t) => t.as_mut(),
            Self::T10(t) => t.as_mut(),
            Self::T11(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> AsRef<T>
    for Enum12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
    T8: AsRef<T>,
    T9: AsRef<T>,
    T10: AsRef<T>,
    T11: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
            Self::T8(t) => t.as_ref(),
            Self::T9(t) => t.as_ref(),
            Self::T10(t) => t.as_ref(),
            Self::T11(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
    T8(T8),
    T9(T9),
    T10(T10),
    T11(T11),
    T12(T12),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> AsMut<T>
    for Enum13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
    T8: AsMut<T>,
    T9: AsMut<T>,
    T10: AsMut<T>,
    T11: AsMut<T>,
    T12: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
            Self::T8(t) => t.as_mut(),
            Self::T9(t) => t.as_mut(),
            Self::T10(t) => t.as_mut(),
            Self::T11(t) => t.as_mut(),
            Self::T12(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> AsRef<T>
    for Enum13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
    T8: AsRef<T>,
    T9: AsRef<T>,
    T10: AsRef<T>,
    T11: AsRef<T>,
    T12: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
            Self::T8(t) => t.as_ref(),
            Self::T9(t) => t.as_ref(),
            Self::T10(t) => t.as_ref(),
            Self::T11(t) => t.as_ref(),
            Self::T12(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
    T8(T8),
    T9(T9),
    T10(T10),
    T11(T11),
    T12(T12),
    T13(T13),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> AsMut<T>
    for Enum14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
    T8: AsMut<T>,
    T9: AsMut<T>,
    T10: AsMut<T>,
    T11: AsMut<T>,
    T12: AsMut<T>,
    T13: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
            Self::T8(t) => t.as_mut(),
            Self::T9(t) => t.as_mut(),
            Self::T10(t) => t.as_mut(),
            Self::T11(t) => t.as_mut(),
            Self::T12(t) => t.as_mut(),
            Self::T13(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> AsRef<T>
    for Enum14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
    T8: AsRef<T>,
    T9: AsRef<T>,
    T10: AsRef<T>,
    T11: AsRef<T>,
    T12: AsRef<T>,
    T13: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
            Self::T8(t) => t.as_ref(),
            Self::T9(t) => t.as_ref(),
            Self::T10(t) => t.as_ref(),
            Self::T11(t) => t.as_ref(),
            Self::T12(t) => t.as_ref(),
            Self::T13(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
    T8(T8),
    T9(T9),
    T10(T10),
    T11(T11),
    T12(T12),
    T13(T13),
    T14(T14),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> AsMut<T>
    for Enum15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
    T8: AsMut<T>,
    T9: AsMut<T>,
    T10: AsMut<T>,
    T11: AsMut<T>,
    T12: AsMut<T>,
    T13: AsMut<T>,
    T14: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
            Self::T8(t) => t.as_mut(),
            Self::T9(t) => t.as_mut(),
            Self::T10(t) => t.as_mut(),
            Self::T11(t) => t.as_mut(),
            Self::T12(t) => t.as_mut(),
            Self::T13(t) => t.as_mut(),
            Self::T14(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> AsRef<T>
    for Enum15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
    T8: AsRef<T>,
    T9: AsRef<T>,
    T10: AsRef<T>,
    T11: AsRef<T>,
    T12: AsRef<T>,
    T13: AsRef<T>,
    T14: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
            Self::T8(t) => t.as_ref(),
            Self::T9(t) => t.as_ref(),
            Self::T10(t) => t.as_ref(),
            Self::T11(t) => t.as_ref(),
            Self::T12(t) => t.as_ref(),
            Self::T13(t) => t.as_ref(),
            Self::T14(t) => t.as_ref(),
        }
    }
}

#[derive(Debug)]
pub enum Enum16<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> {
    T0(T0),
    T1(T1),
    T2(T2),
    T3(T3),
    T4(T4),
    T5(T5),
    T6(T6),
    T7(T7),
    T8(T8),
    T9(T9),
    T10(T10),
    T11(T11),
    T12(T12),
    T13(T13),
    T14(T14),
    T15(T15),
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> AsMut<T>
    for Enum16<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
where
    T0: AsMut<T>,
    T1: AsMut<T>,
    T2: AsMut<T>,
    T3: AsMut<T>,
    T4: AsMut<T>,
    T5: AsMut<T>,
    T6: AsMut<T>,
    T7: AsMut<T>,
    T8: AsMut<T>,
    T9: AsMut<T>,
    T10: AsMut<T>,
    T11: AsMut<T>,
    T12: AsMut<T>,
    T13: AsMut<T>,
    T14: AsMut<T>,
    T15: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        match self {
            Self::T0(t) => t.as_mut(),
            Self::T1(t) => t.as_mut(),
            Self::T2(t) => t.as_mut(),
            Self::T3(t) => t.as_mut(),
            Self::T4(t) => t.as_mut(),
            Self::T5(t) => t.as_mut(),
            Self::T6(t) => t.as_mut(),
            Self::T7(t) => t.as_mut(),
            Self::T8(t) => t.as_mut(),
            Self::T9(t) => t.as_mut(),
            Self::T10(t) => t.as_mut(),
            Self::T11(t) => t.as_mut(),
            Self::T12(t) => t.as_mut(),
            Self::T13(t) => t.as_mut(),
            Self::T14(t) => t.as_mut(),
            Self::T15(t) => t.as_mut(),
        }
    }
}

impl<T, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> AsRef<T>
    for Enum16<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
where
    T0: AsRef<T>,
    T1: AsRef<T>,
    T2: AsRef<T>,
    T3: AsRef<T>,
    T4: AsRef<T>,
    T5: AsRef<T>,
    T6: AsRef<T>,
    T7: AsRef<T>,
    T8: AsRef<T>,
    T9: AsRef<T>,
    T10: AsRef<T>,
    T11: AsRef<T>,
    T12: AsRef<T>,
    T13: AsRef<T>,
    T14: AsRef<T>,
    T15: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::T0(t) => t.as_ref(),
            Self::T1(t) => t.as_ref(),
            Self::T2(t) => t.as_ref(),
            Self::T3(t) => t.as_ref(),
            Self::T4(t) => t.as_ref(),
            Self::T5(t) => t.as_ref(),
            Self::T6(t) => t.as_ref(),
            Self::T7(t) => t.as_ref(),
            Self::T8(t) => t.as_ref(),
            Self::T9(t) => t.as_ref(),
            Self::T10(t) => t.as_ref(),
            Self::T11(t) => t.as_ref(),
            Self::T12(t) => t.as_ref(),
            Self::T13(t) => t.as_ref(),
            Self::T14(t) => t.as_ref(),
            Self::T15(t) => t.as_ref(),
        }
    }
}
